<?php

declare(strict_types=1);

namespace DigitalBoutique\Redirector\Controller\Product;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Psr\Log\LoggerInterface as Logger;
use DigitalBoutique\Redirector\Model\SearchLogFactory;
use DigitalBoutique\Redirector\Model\SearchLogRepository;
use DigitalBoutique\Redirector\Model\SearchLog;

class Redirect implements HttpPostActionInterface
{
    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var MessageManagerInterface
     */
    private MessageManagerInterface $messageManager;

    /**
     * @var SearchLogFactory
     */
    private $searchLogFactory;

    /**
     * @var SearchLogRepository
     */
    private $searchLogRepository;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Redirect constructor.
     * @param RequestInterface $request
     * @param RedirectFactory $redirectFactory
     * @param ProductRepositoryInterface $productRepository
     * @param MessageManagerInterface $messageManager
     * @param SearchLogFactory $searchLogFactory
     * @param SearchLogRepository $searchLogRepository
     * @param Logger $logger
     */
    public function __construct(
        RequestInterface $request,
        RedirectFactory $redirectFactory,
        ProductRepositoryInterface $productRepository,
        MessageManagerInterface $messageManager,
        SearchLogFactory $searchLogFactory,
        SearchLogRepository $searchLogRepository,
        Logger $logger
    ) {
        $this->request = $request;
        $this->redirectFactory = $redirectFactory;
        $this->productRepository = $productRepository;
        $this->messageManager = $messageManager;
        $this->searchLogFactory = $searchLogFactory;
        $this->searchLogRepository = $searchLogRepository;
        $this->logger = $logger;
    }

    /**
     * @return ResultInterface
     */
    public function execute()
    {
        if (!$productSku = $this->request->getParam('product_sku')) {
            $this->messageManager->addErrorMessage('Please input product SKU.');
            return $this->redirectFactory->create()->setPath('digital-boutique/m2-test');
        }

        try {
            $product = $this->productRepository->get((string) $productSku);
            if (!$product->isVisibleInSiteVisibility()) {
                $this->messageManager->addErrorMessage('This product does not exist.');
                return $this->redirectFactory->create()->setPath('digital-boutique/m2-test');
            }

            $this->logSearchEntry($productSku);

            $urlStore = $product->getUrlInStore(); // or url key
            return $this->redirectFactory->create()->setPath($urlStore);
        } catch (Exception $exception) {
            $this->messageManager->addErrorMessage('This product does not exist.');
            return $this->redirectFactory->create()->setPath('digital-boutique/m2-test');
        }
    }

    /**
     * Log search entry from the form
     * @param string $sku
     * @return void
     */
    public function logSearchEntry(string $sku): void
    {
        try {
            /** @var SearchLog $searchLog */
            $searchLog = $this->searchLogFactory->create();
            $searchLog->setProductSku($sku);
            $this->searchLogRepository->save($searchLog);
        } catch (\Exception $e) {
            $this->logger->error('Unable to log search entry for sku: ' . $sku . ', Error: ' . $e->getMessage());
        }
    }
}
