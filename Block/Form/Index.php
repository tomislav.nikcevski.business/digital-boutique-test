<?php

declare(strict_types=1);

namespace DigitalBoutique\Redirector\Block\Form;

use Magento\Framework\View\Element\Template;

/**
 * Class Index
 *
 * Block for m2-test form, alternative is to use ViewModel, but Block has built in Url function convenient for this
 */
class Index extends Template
{
    /**
     * Get redirect submit action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->getUrl(
            'digital-boutique/product/redirect',
            [
                '_secure' => $this->getRequest()->isSecure(),
            ]
        );
    }
}
