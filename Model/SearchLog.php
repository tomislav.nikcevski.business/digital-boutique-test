<?php

declare(strict_types=1);

namespace DigitalBoutique\Redirector\Model;

use Magento\Framework\Model\AbstractModel;
use DigitalBoutique\Redirector\Model\ResourceModel\SearchLog as SearchLogResource;

/**
 * Class SearchLog
 *
 * Possibility to define model here via API interface, not used due to time
 */
class SearchLog extends AbstractModel
{
    /**
     * Table columns
     */
    const ID = 'id';
    const PRODUCT_SKU = 'product_sku';
    const SEARCHED_AT = 'searched_at';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SearchLogResource::class);
    }

    /**
     * @return array|mixed|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @param mixed $id
     * @return SearchLog
     */
    public function setId($id): SearchLog
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @param string $sku
     * @return SearchLog
     */
    public function setProductSku(string $sku): SearchLog
    {
        return $this->setData(self::PRODUCT_SKU, $sku);
    }

    /**
     * @return string|null
     */
    public function getProductSku(): ?string
    {
        return $this->getData(self::PRODUCT_SKU);
    }

    /**
     * @param string $date
     * @return SearchLog
     */
    public function setSearchedAt(string $date): SearchLog
    {
        return $this->setData(self::SEARCHED_AT, $date);
    }

    /**
     * @param string $date
     * @return string
     */
    public function getSearchedAt(string $date): string
    {
        return $this->getData(self::SEARCHED_AT);
    }
}
