<?php

declare(strict_types=1);

namespace DigitalBoutique\Redirector\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SearchLog extends AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('digital_boutique_redirector_search_log', 'id');
    }
}
