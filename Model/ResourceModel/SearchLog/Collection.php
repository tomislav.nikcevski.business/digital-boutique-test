<?php

declare(strict_types=1);

namespace DigitalBoutique\Redirector\Model\ResourceModel\SearchLog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use DigitalBoutique\Redirector\Model\SearchLog;
use DigitalBoutique\Redirector\Model\ResourceModel\SearchLog as SearchLogResource;

/**
 * Class Collection
 *
 * Not mandatory for this task, but good to have
 */
class Collection extends AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            SearchLog::class,
            SearchLogResource::class
        );
        $this->_setIdFieldName('id');
    }
}
