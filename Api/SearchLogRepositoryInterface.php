<?php

declare(strict_types=1);

namespace DigitalBoutique\Redirector\Api;

use DigitalBoutique\Redirector\Model\SearchLog;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

interface SearchLogRepositoryInterface
{
    /**
     * @param mixed $id
     * @return SearchLog
     * @throws NoSuchEntityException
     */
    public function get($id): SearchLog;

    /**
     * @param SearchLog $searchLog
     * @return SearchLog
     * @throws CouldNotSaveException
     */
    public function save(SearchLog $searchLog): SearchLog;

    /**
     * @param SearchLog $searchLog
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(SearchLog $searchLog): bool;
}
